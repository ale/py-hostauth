import json
import subprocess
import time
import urllib2

from OpenSSL import crypto


pki_helper_binary = 'pki-helper'
default_hostd_port = 2024


def get_host_certificate(port=None):
    if not port:
        port = hostd_port
    resp = urllib2.urlopen('http://localhost:%d/_well_known/host.crt' % port)
    crt = crypto.load_certificate(crypto.FILETYPE_ASN1, resp.read())
    expires = time.strptime('%Y%m%d%H%M%S', crt.get_notAfter())
    return crt, expires


class Error(Exception):
    pass


def generate_certificate(service=None):
    # Bummer: pyOpenSSL can't generate ECDSA keys. Shell out to the
    # `pki-helper' command-line tool.
    cmd = [pki_helper_binary, '--json', '--client']
    if service:
        cmd.append('--service=%s' % service)
    try:
        data = json.loads(subprocess.check_output(cmd))
    except (ValueError, subprocess.CalledProcessError) as e:
        raise Error('Error generating certificate: %s' % str(e))

    for attr in ('cert', 'key', 'expires'):
        if attr not in data:
            raise Error('Error generating certificate: missing attribute %s' % attr)

    crt = crypto.load_certificate(crypto.FILETYPE_ASN1, data['cert'])
    key = crypto.load_privatekey(crypto.FILETYPE_ASN1, data['key'])
    expires = time.strptime(data['expires'], '%Y-%m-%dT%H:%M:%S')
    return crt, key, expires


import os
import mock
import subprocess
import unittest
import time

import hostauth
from OpenSSL import crypto


datadir = os.path.join(os.path.dirname(__file__), 'testdata')


class ClientTest(unittest.TestCase):

    def setUp(self):
        super(ClientTest, self).setUp()
        self._mp = mock.patch('hostauth.Client._update_host_cert',
                              return_value=42)
        self._mp.start()
        self._cp = mock.patch('hostauth.Client._update_client_cert',
                              return_value=42)
        self._cp.start()

        self.client = hostauth.Client(
            service='test',
            root_ca=os.path.join(datadir, 'ca.pem'),
            )

        with open(os.path.join(datadir, 'client.crt')) as fd:
            self.client.client_crt = crypto.load_certificate(crypto.FILETYPE_ASN1, fd.read())
        with open(os.path.join(datadir, 'client.key')) as fd:
            self.client.client_key = crypto.load_privatekey(crypto.FILETYPE_ASN1, fd.read())
        with open(os.path.join(datadir, 'hostca.crt')) as fd:
            self.client.host_crt = crypto.load_certificate(crypto.FILETYPE_ASN1, fd.read())

        # Start server.
        devnull = open('/dev/null', 'r')
        self.server = subprocess.Popen(
            ['openssl', 's_server', '-accept', '4433',
             '-cert', os.path.join(datadir, 'server.pem'), '-certform', 'PEM',
             '-msg', '-state',
             '-key', os.path.join(datadir, 'server.key'), '-keyform', 'DER',
             '-CAfile', os.path.join(datadir, 'hostca-chained.pem'),
             '-tls1', '-www'],
            stdin=devnull)
        time.sleep(0.5)

    def tearDown(self):
        self.client.close()
        self.server.terminate()
        self.server.wait()
        self._mp.stop()
        self._cp.stop()
        super(ClientTest, self).tearDown()

    def test_main(self):
        url = 'https://localhost:4433/'
        data = self.client.urlopen(url).read()
        print data
        

catool=${CATOOL:-pki-catool}
$catool --init
mv out.key ca.key
mv out.crt ca.crt
openssl x509 -in ca.crt -inform DER -out ca.pem -outform PEM
$catool --ca --out-crt=hostca.crt --out-key=hostca.key
openssl x509 -in hostca.crt -inform DER -out hostca.pem -outform PEM
$catool --client --ca-crt=hostca.crt --ca-key=hostca.key \
    --out-crt=client.crt --out-key=client.key
$catool --server --ca-crt=hostca.crt --ca-key=hostca.key \
    --out-crt=server.crt --out-key=server.key localhost
cat hostca.pem ca.pem > hostca-chained.pem

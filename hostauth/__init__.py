import logging
import os
import threading
import time
import urlparse

from OpenSSL import SSL

from hostauth import proto
from hostauth.httpsclient.urllib2_build_opener import build_opener

log = logging.getLogger(__name__)


class _renewable(object):

    def __init__(self, deadline, fn):
        self._stop = threading.Event()
        self._deadline = deadline
        self._fn = fn
        self._t = threading.Thread(target=self.run)
        self._t.start()

    def stop(self):
        self._stop.set()
        self._t.join()

    def run(self):
        while True:
            self._stop.wait(timeout=time.time()-self._deadline)
            while True:
                try:
                    self._deadline = self._fn()
                    break
                except Exception as e:
                    log.error('error renewing certificate: %s', e)
                    time.sleep(10)


def _expiration_deadline(expires):
    now = time.time()
    return now + (expires - now) * 0.8


class Client(object):

    def __init__(self, service=None, root_ca='/etc/authd/root.crt',
                 hostd_port=proto.default_hostd_port,
                 hostd_socket='/var/run/authd/local'):
        self.root_ca = root_ca
        self.hostd_port = hostd_port
        self.hostd_socket = hostd_socket
        self.service = service

        self._lock = threading.Lock()
        self.host_crt = None
        self.client_crt = None
        self.client_key = None

        hostexp = self._update_host_cert()
        self.host_renew = _renewable(hostexp, self._update_host_cert)
        clientexp = self._update_client_cert()
        self.client_renew = _renewable(clientexp, self._update_client_cert)

        self._opener = build_opener(ssl_context_fn=self.make_ssl_context)

    def close(self):
        self.host_renew.stop()
        self.client_renew.stop()

    def _update_host_cert(self):
        crt, expires = proto.get_host_certificate(self.hostd_port)
        with self._lock:
            self.host_crt = crt
        return _expiration_deadline(expires)

    def _update_client_cert(self):
        crt, key, expires = proto.generate_certificate(service=self.service)
        with self._lock:
            self.client_crt = crt
            self.client_key = key
        return _expiration_deadline(expires)

    def make_ssl_context(self, url):
        ssl_context = SSL.Context(SSL.TLSv1_METHOD)

        with self._lock:
            ssl_context.use_privatekey(self.client_key)
            ssl_context.use_certificate(self.client_crt)
            ssl_context.add_extra_chain_cert(self.host_crt)

        ssl_context.load_verify_locations(self.root_ca, None)
        ssl_context.set_verify_depth(2)
        _set_peer_verification_for_url(ssl_context, url)

        return ssl_context

    def urlopen(self, req):
        return self._opener.open(req)


def _set_peer_verification_for_url(ssl_context, url):
    urlobj = urlparse.urlparse(url)
    hostport = urlobj.hostname

    def _callback(conn, peer_cert, errnum, errdepth, preverify_ok):
        if peer_cert.has_expired():
            log.error('certificate %r has expired', peer_cert.get_subject())
            return False
        elif errdepth == 0:
            subj = peer_cert.get_subject()
            if subj.commonName != hostport:
                log.error('certificate CN %r does not match expected CN %s',
                          subj, hostport)
                return preverify_ok
        return preverify_ok

    ssl_context.set_verify(SSL.VERIFY_PEER, _callback)

#!/usr/bin/python

from setuptools import setup, find_packages

setup(
    name='hostauth',
    version='0.1',
    description='Python client for hostauth.',
    packages=find_packages(),
    install_requires=['PyOpenSSL'],
    entry_points={
        'console_scripts': [
        ],
    },
)
